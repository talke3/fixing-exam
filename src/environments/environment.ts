// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyBL6i1gthWpf-xjItC1hFrhk3Jw9SsPxJM",
    authDomain: "projectname-3bedf.firebaseapp.com",
    projectId: "projectname-3bedf",
    storageBucket: "projectname-3bedf.appspot.com",
    messagingSenderId: "136198486945",
    appId: "1:136198486945:web:1fc1eef4ba247ee554e9e0"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
