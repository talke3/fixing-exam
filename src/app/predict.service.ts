import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PredictService {

  private url = "https://krrv6e9xo3.execute-api.us-east-1.amazonaws.com/a";

  predict(averageMath: number, psychoGrade: number, completeTuition: boolean):Observable<any>{
    let json = {
      "data": 
        {
          "averageMath": averageMath,
          "psychoGrade": psychoGrade,
          "completeTuition":completeTuition
        }
    }
    let body  = JSON.stringify(json);
    return this.http.post<any>(this.url,body).pipe(
      map(res => {
        return res.body;       
      })
    );      
  }



  
  constructor(private http: HttpClient) { }
}