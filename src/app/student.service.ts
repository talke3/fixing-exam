import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {AngularFirestore, AngularFirestoreCollection} from '@angular/fire/firestore';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class StudentService {
  studentCollection:AngularFirestoreCollection  = this.db.collection(`/students`); ;
  userCollection:AngularFirestoreCollection = this.db.collection('users'); 

  public getStudent(){
    return this.studentCollection.snapshotChanges()      
  } 

  
  deleteStudent(id:string){
    this.db.doc(`/students/${id}`).delete(); 
  } 

  
  addStudent(averageMath:number,psychoGrade:number,completeTuition:boolean,result:string,useremail:string){
    const student = {averageMath:averageMath,psychoGrade:psychoGrade,completeTuition:completeTuition,result:result,useremail:useremail}; 
    this.studentCollection.add(student);
    this.router.navigate(['/studentdata']);
  }


  updateStudent(id:string,averageMath:number,psychoGrade:number,completeTuition:boolean,result:string){
    this.db.doc(`/students/${id}`).update(
      {
        averageMath:averageMath,
        psychoGrade:psychoGrade,
        completeTuition:completeTuition,
        result:result,

      }
    )
  }
  


  constructor(private db:AngularFirestore, public router:Router, private http:HttpClient) { }
}
