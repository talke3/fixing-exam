import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SignUpComponent } from './sign-up/sign-up.component';
import { LoginComponent } from './login/login.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { StudentdataFormComponent } from './studentdata-form/studentdata-form.component';
import { StudentdataComponent } from './studentdata/studentdata.component';
import { CustomerComponent } from './customer/customer.component';
import { CustomersComponent } from './customers/customers.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent},
  { path: 'signup', component: SignUpComponent},
  { path: 'welcome', component: WelcomeComponent},
  { path: 'studentdataform', component: StudentdataFormComponent},
  { path: 'studentdata', component: StudentdataComponent},
  { path: 'customer', component: CustomerComponent},
  { path: 'customers', component: CustomersComponent}
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

