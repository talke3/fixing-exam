import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Customer } from '../interfaces/customer';
import { AngularFirestore } from '@angular/fire/firestore';
import { CustomerService } from '../customer.service';

@Component({
  selector: 'app-customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.css']
})
export class CustomersComponent implements OnInit {
  customers$;
  customers:Customer[];
  customersId:string; 
  userId:string; 

  deleteStudent(id:string){
    this.customerService.deleteCustomer(id);
  }

  constructor(private customerService:CustomerService, public authService:AuthService,private db:AngularFirestore) { }

  ngOnInit(): void {
    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid;
        console.log(this.userId); 
        this.customers$ = this.customerService.getCustomer(this.userId);

      this.customers$.subscribe(
      docs =>{
        console.log('worked');           
        this.customers = [];
        for(let document of docs){
          const customer:Customer = document.payload.doc.data();
          customer.id = document.payload.doc.id; 
          this.customers.push(customer); 
        }
      }
    ) 
  }
)

}

}