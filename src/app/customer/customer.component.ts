import { CustomerService } from './../customer.service';
import { Component, EventEmitter, OnInit } from '@angular/core';
import { Customer } from '../interfaces/customer';

@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.css']
})
export class CustomerComponent implements OnInit {

  customerName:string;
  yearsStudy:number;
  income:number;
  id:string;
  predictButton:boolean = true
  isError:boolean = false;
  update = new EventEmitter<Customer>();

  
  closeForm(){
    this.predictButton = true;
    this.customerName = null;
    this.yearsStudy = null;
    this.income = null;
  }

  addCustomer(){
    this.customerService.addCustomer(this.customerName, this.yearsStudy, this.income);
    let customer:Customer = {id:this.id, customerName:this.customerName, yearsStudy:this.yearsStudy,income:this.income};
    if(this.yearsStudy>24 || this.yearsStudy<0){
      this.isError = true;
    }else{
      this.update.emit(customer);  
    }
  }

  constructor(private customerService:CustomerService,) { }

  ngOnInit(): void {
  }

}
