export interface Customer {
  id:string;
  customerName:string;
  yearsStudy:number;
  income:number;
}
