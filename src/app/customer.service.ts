import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {AngularFirestore, AngularFirestoreCollection} from '@angular/fire/firestore';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {

  customerCollection:AngularFirestoreCollection; 
  userCollection:AngularFirestoreCollection = this.db.collection('users'); 

  public getCustomer(userId){
    this.customerCollection = this.db.collection(`users/${userId}/customers`)
    return this.customerCollection.snapshotChanges()      
  } 

  
  deleteCustomer(id:string){
    this.db.doc(`/customer/${id}`).delete(); 
  } 

  
  addCustomer(customerName:string,yearsStudy:number,income:number){
    const customer = {customerName:customerName,yearsStudy:yearsStudy,income:income}; 
    this.customerCollection.add(customer);
    this.router.navigate(['/customers']);
  }


  updateCustomer(id:string,customerName:string,yearsStudy:number,income:number){
    this.db.doc(`/customers/${id}`).update(
      {
        customerName:customerName,
        yearsStudy:yearsStudy,
        income:income
      }
    )
  }
  
  constructor(private db:AngularFirestore, public router:Router, private http:HttpClient) { }
}