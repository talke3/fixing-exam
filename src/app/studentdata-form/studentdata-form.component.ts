import { StudentService } from './../student.service';
import { Component, EventEmitter, OnInit } from '@angular/core';
import { Student } from '../interfaces/student';
import { PredictService } from '../predict.service';
import { PriceService } from '../price.service';


@Component({
  selector: 'app-studentdata-form',
  templateUrl: './studentdata-form.component.html',
  styleUrls: ['./studentdata-form.component.css']
})
export class StudentdataFormComponent implements OnInit {

  averageMath=80;
  psychoGrade:number;
  completeTuition:boolean;
  tax = 130;
  mpg = 55.5;
  engineSize = 2;
  year=2020;
  mileage=500;
  model:string;
  transmission:string;
  fuelType:string;
  model_1_Series=0;
  model_2_Series=0;
  model_3_Series=0;
  model_4_Series=0;
  model_5_Series=0;
  transmission_Automatic=0;	
  transmission_Manual=0;	
  transmission_Semi_Auto=0;	
  fuelType_Diesel=0;	
  fuelType_Petrol=0;
  res:number=0;
  red;	
  useremail:string;
  id:string;
  arrPayed:Array<boolean> = [true, false];
  arrRes:Array<string> = ['Finish', 'Will not finish'];
  arrModel:Array<string> = ['model_1_Series', 'model_2_Series','model_3_Series','model_4_Series','model_5_Series'];
  arrTrans:Array<string> = ['transmission_Automatic', 'transmission_Manual','transmission_Semi-Auto'];
  arrFuel:Array<string> = ['fuelType_Diesel', 'fuelType_Petrol'];
  result:string;
  predictButton:boolean = true
  isError:boolean = false;
  update = new EventEmitter<Student>();


  closeForm(){
    this.predictButton = true;
    this.averageMath = null;
    this.psychoGrade = null;
    this.completeTuition = null;
    this.useremail = null;
  }

  addStudent(){
    this.studentService.addStudent(this.averageMath, this.psychoGrade, this.completeTuition,this.result,this.useremail);
    let student:Student = {id:this.id, averageMath:this.averageMath, psychoGrade:this.psychoGrade,completeTuition:this.completeTuition,useremail:this.useremail};
    if(this.averageMath>100 || this.averageMath<0 ||this.psychoGrade>800 || this.psychoGrade<0){
      this.isError = true;
    }else{
      this.update.emit(student);  
    }
  }

  predict(){
    if(this.model='model_1_Series'){
      this.model_1_Series=1;
    }
    if(this.model='model_2_Series'){
      this.model_2_Series=1;
    }
    if(this.model='model_3_Series'){
      this.model_3_Series=1;
    }
    if(this.model='model_4_Series'){
      this.model_4_Series=1;
    }
    if(this.model='model_5_Series'){
      this.model_5_Series=1;
    }
    if(this.transmission='transmission_Automatic'){
      this.transmission_Automatic=1;
    }
    if(this.transmission='transmission_Manual'){
      this.transmission_Manual=1;
    }
    if(this.transmission='transmission_Semi_Auto'){
      this.transmission_Semi_Auto=1;
    }
    if(this.fuelType='fuelType_Diesel'){
      this.fuelType_Diesel=1;
    }
    if(this.fuelType='fuelType_Petrol'){
      this.fuelType_Petrol=1;
    }
    this.priceService.predict(this.year, this.mileage, this.tax, this.mpg, this.engineSize,this.model_1_Series,this.model_2_Series,this.model_3_Series,this.model_4_Series,this.model_5_Series,this.transmission_Automatic,this.transmission_Manual,this.transmission_Semi_Auto,this.fuelType_Diesel,this.fuelType_Petrol).subscribe(
      res => { 
        console.log(this.model_1_Series);
        console.log(this.transmission_Automatic);
        console.log(this.fuelType_Diesel);
        console.log(res);
        this.res=res;
        this.predictButton = false;
      ;}
    ); 
  }



  constructor(private studentService:StudentService,private predictService:PredictService,private priceService:PriceService) { }

  ngOnInit(): void {
  }

}

