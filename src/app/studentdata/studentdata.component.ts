import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Student } from '../interfaces/student';
import { AngularFirestore } from '@angular/fire/firestore';
import { StudentService } from '../student.service';

@Component({
  selector: 'app-studentdata',
  templateUrl: './studentdata.component.html',
  styleUrls: ['./studentdata.component.css']
})
export class StudentdataComponent implements OnInit {

  students$;
  students:Student[];
  studentId:string; 

  deleteStudent(id:string){
    this.studentService.deleteStudent(id);
  }

  constructor(private studentService:StudentService, public authService:AuthService,private db:AngularFirestore) { }

  ngOnInit(): void {
    this.students$ = this.studentService.getStudent(); 
    this.students$.subscribe(
      docs =>{
        console.log('worked');           
        this.students = [];
        for(let document of docs){
          const student:Student = document.payload.doc.data();
          student.id = document.payload.doc.id; 
          this.students.push(student); 
        }
      }
    ) 
}

}
